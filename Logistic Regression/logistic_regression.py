'''
    Author: Abdurrezak Efe
    Date: 26.04.2022
'''


from asyncio.windows_events import INFINITE
import numpy as np

class LogisticRegression():

    def __init__(self):
        self.t0 = 0
        self.t1 = 0
        self.eps = 0.001
        self.x = None
        self.y = None
        self.losses = []
        self.n = 0

    def fit(self, x, y):   
        self.x = x
        self.y = y
        self.n = len(self.x)
        self.gradient_descent()

    def predict(self, x, alpha = 0.5):
        ans = []
        if type(x) == list:
            for x_ in x:
                ans.append(1 if self.sigmoid(x_) > alpha else 0)
        else:
            return 1 if self.sigmoid(x) > alpha else 0

        return ans 

    def sigmoid(self, z):
        
        sig = 1 / (1 + np.exp(-self.t0 - self.t1*z))
        # print(sig, self.t0, self.t1)
        return sig

    def gradient_descent(self, learning_rate = 1e-5):
        prev = INFINITE
        cur_loss = self.nll()
        # while(np.abs(cur_loss - prev) > self.eps/10000):
        stp = 0
        while stp < 50000:
            stp += 1
            prev = cur_loss
            dldt0 = 0
            dldt1 = 0
            for i in range(self.n):
                dldt0 += self.sigmoid(self.x[i]) - self.y[i]
                dldt1 += self.x[i]*(self.sigmoid(self.x[i]) - self.y[i])

            self.t0 = self.t0 - learning_rate*dldt0
            self.t1 = self.t1 - learning_rate*dldt1
            cur_loss = self.nll()
            if stp % 1000 == 0:
                print("LOSS:" , cur_loss)
            
    def nll(self):
        loss = 0
        for i, x in enumerate(self.x):
            loss += self.y[i]*np.log(self.sigmoid(x)) + (1-self.y[i])*np.log(1 - self.sigmoid(x))
        return -loss / len(self.x)


# a simple example

import random 

random.seed(0)

x = []
y = []

for i in range(100):
    x.append(i + random.random())
    if i < 50:
        y.append(0)
    else:
        y.append(1)

from sklearn.model_selection import train_test_split

print("Start")
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)

cl = LogisticRegression()
cl.fit(x_train, y_train)
preds = cl.predict(x_test)

print("Predictions:", preds)
print("Real labels:", y_test)
print("Classification score:", np.sum(np.array(preds) == np.array(y_test)) / len(y_test))

