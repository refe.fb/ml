from sklearn.linear_model import LogisticRegression
import random 
import numpy as np

random.seed(0)

x = []
y = []

for i in range(100):
    x.append(i + random.random())
    if i < 50:
        y.append(0)
    else:
        y.append(1)

from sklearn.model_selection import train_test_split

print("Start")
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)

clf = LogisticRegression(random_state=0).fit(np.array(x_train).reshape(-1,1), np.array(y_train))

preds = clf.predict(np.array(x_test).reshape(-1,1))

print(clf.score(np.array(preds).reshape(-1,1), y_test))