'''
    Author: Abdurrezak Efe
    Date: 23.04.2022
'''

from cProfile import label
from cmath import inf
import numpy as np
import matplotlib.pylab as plt
import random 

class Linear_Regression():

    def __init__(self):
        self.x = None
        self.y = None
        self.m = 0
        self.b = 0
        self.n = 0
        self.ms = []
        self.mses = []
        self.eps = []
        self.bs = []

    def fit(self, x, y, viz):
        self.x = x 
        self.y = y
        self.m = np.random.random()
        self.b = np.random.random()
        self.n = len(x)
        self.gradient_descent(visualize=viz)

    def gradient_descent(self, learning_rate = 1e-5, visualize = False):

        if(visualize):
            plt.rcParams["figure.figsize"] = (10,10)
            figure, axis = plt.subplots(2,2)

        prev_mse = inf 
        cur_mse = self.mse(self.y, self.predict(self.x))
        i = 0
        while abs(cur_mse - prev_mse) > 0.1:
            
            prev_mse = cur_mse

            if(visualize):
                if(i%31 == 0):
                    axis[0,0].clear()
                    axis[0,1].clear()
                    axis[1,0].clear()
                    axis[1,1].clear()
                    axis[0,0].scatter(self.x, self.y, color="black")      
                    axis[0,0].plot(self.x, self.predict(self.x), color="blue", linewidth=1, label="Fit line")
                    axis[0,0].set_xlabel("X")
                    axis[0,0].set_ylabel("Y")
                    axis[0,0].legend(loc="upper left")
                    axis[0,1].plot(self.ms, [-1*smse for smse in self.mses], color = "purple", label="m")
                    axis[0,1].set_xlabel("m and b")
                    axis[0,1].plot(self.bs, [-1*smse for smse in self.mses], color = "pink", label="b")
                    axis[0,1].set_ylabel("-MSE")
                    axis[0,1].legend(loc="lower right")
                    axis[1,0].plot(range(len(self.mses)), self.mses, color = "green")
                    axis[1,0].set_xlabel("iteration")
                    axis[1,0].set_ylabel("MSE")
                    axis[1,1].plot(range(len(self.eps)),self.eps, color="red")
                    axis[1,1].set_xlabel("iteration")
                    axis[1,1].set_ylabel("Epsilon")
                    plt.pause(0.0001)

            dldm = 0
            dldb = 0
            
            # calculating loss function derivative with m and b
            for i in range(self.n):
                dldm += self.x[i]*(self.m*self.x[i] + self.b - self.y[i])
                dldb += (self.b + self.m*self.x[i]- self.y[i])

            dldm /= self.n
            dldb /= self.n

            self.m -= learning_rate*dldm
            self.b -= learning_rate*dldb

            cur_mse = self.mse(self.y, self.predict(self.x))
            self.mses.append(cur_mse)
            self.ms.append(self.m)
            self.eps.append(abs(cur_mse - prev_mse))
            self.bs.append(self.b)
            i += 1

            print("MSE:", cur_mse)
            
    def predict(self, x):
        # this function makes individual and list predictions

        if type(x) == list:
            ans = []
            for x_ in x:
                ans.append(self.m*x_ + self.b)
            return ans  
        else:
            return self.m*x + self.b

    def mse(self, y, yhat):
        # here we calculate mse for original and predicted data
        num = len(y)
        mse_ = 0 
        for i, y_ in enumerate(y):
            mse_ += (y_ - yhat[i])**2
        
        return mse_/(num)


# a simple example below
random.seed(0)

viz = False 
x_ = []
y_ = []

for i in range(100):
    x_.append(i)
    y_.append(5*i + 3 + 20*random.random())

print(y_)

x_train = x_[0:-20]
y_train = y_[0:-20]

x_test = x_[-20:]
y_test = y_[-20:]

linear_reg = Linear_Regression()

linear_reg.fit(x_train, y_train, viz)

preds = linear_reg.predict(x_test)

print("Test score:", linear_reg.mse(y_test, preds))
print("Coefficient:", linear_reg.m)
print("Intercept:", linear_reg.b)

if(True):
    plt.scatter(x_test, y_test, color="black")      
    plt.plot(x_test, preds, color="blue", linewidth=1, label="Fit line")
    plt.show()
