'''
    Author: Abdurrezak Efe
    Date: 23.04.2022
'''

import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
import random
from sklearn.metrics import mean_squared_error, r2_score

random.seed(0)

x_ = []
y_ = []

for i in range(100):
    x_.append(i)
    y_.append(5*i + 3 + 20*random.random())

x_ = np.array(x_).reshape(-1,1)
x_train = x_[0:-20]
y_train = y_[0:-20]

x_test = x_[-20:]
y_test = y_[-20:]

regr = linear_model.LinearRegression()

regr.fit(x_train, y_train)

y_pred = regr.predict(x_test)

# The coefficients
print("Coefficient:", regr.coef_)
print("Intercept:", regr.intercept_)

print("Mean squared error: %.2f" % mean_squared_error(y_test, y_pred))
print("Coefficient of determination: %.2f" % r2_score(y_test, y_pred))

plt.scatter(x_test, y_test, color="black")
plt.plot(x_test, y_pred, color="blue", linewidth=1)
plt.show()