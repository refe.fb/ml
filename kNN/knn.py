'''
    Author: Abdurrezak Efe
    Date: 26.04.2022
    updated
'''

import numpy as np
import random
import matplotlib.pylab as plt

class knn():

    def __init__(self, x, y, k):
        self.k = k
        self.n = 0
        self.x = x 
        self.y = y  
        self.classes = np.unique(self.y)

    def predict(self, x):

        preds = []
        for x_p in x:
            
            distances = []
            
            for i, x_ in enumerate(self.x):
                dist = np.sqrt((x_[0] - x_p[0])**2 + (x_[1] - x_p[1])**2)
                distances.append([dist, self.y[i]])
            
            distances = sorted(distances, key=lambda item: item[0])
            
            majors = np.unique(self.y) 

            for i in range(self.k):
                majors[distances[i][1]] += 1

            max_class = 0
            max_elem = 0

            for i in range(len(self.classes)):
                if majors[i] > max_elem:
                    max_elem = majors[i]
                    max_class = i

            preds.append(max_class)
        
        return preds 

# a simple example
random.seed(0)

X = []
y = []

for i in range(100):
    X.append([random.random(), random.random()])
    
    if X[-1][0] < 0.5 and X[-1][1] < 0.5:
        y.append(0)
    elif X[-1][0] >= 0.5 and X[-1][1] >= 0.5:
        y.append(1)
    else:
        y.append(2)

from sklearn.model_selection import train_test_split 

x_train, x_test, y_train, y_test = train_test_split(X, y, random_state=0, test_size=0.2)


clas = knn(x_train, y_train, 10)
prds = clas.predict(x_test)

print("Accuracy:", np.sum(np.array(prds) == np.array(y_test))/len(y_test))

colormap = np.array(['r', 'g', 'b'])

plt.scatter([x[0] for x in x_train], [x[1] for x in x_train], c = colormap[y_train])

plt.scatter([x[0] for x in x_test], [x[1] for x in x_test]) # this is out point that we guess

plt.show()
